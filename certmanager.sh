#!/bin/bash

################################################################################
##  Legal
#
#    Certmanager is a user friendly and interactive script to maintain TLS-Certificates from Let's Encrypt.
#    Copyright (C) 2017  Masgalor
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#    This script contains no code from, but depends on:
#    "acme-tiny.py" Copyright (C) 2015 Daniel Roesler
#    https://github.com/diafygi/acme-tiny/blob/master/LICENSE


################################################################################
##  Functions
#

##  Function "_check_file_access"
#   This function checks if a folder or file exists.
#   It checks also if the given path is accessable in the given way.
#
#   Usage: _check_file_access <Path> <Permissions> <Type>
#          _check_file_access "/etc/certmanager/certmanager.sh" "rwx" "f"
#
_check_file_access(){
#$1 Path to check
#$2 Required Permissions (rwx)
#$3 Optional type directory or file (d/f)
local permission_set="$2"
local type_set="$3"
local array_access_stats
local access_stats_index
local actual_user=$(whoami)
local actual_groups=$(groups)
if ([ ${#permission_set} -lt 1 ] || [ ${#permission_set} -gt 3 ]) ||
   [[ $permission_set =~ [^rwx] ]]; then
  echo "The given permissionset is not valid."
  echo "The expected format is \"r\" - \"rw\" - \"rwx\""
  echo "EXITING"
  return 1
fi
[ "$type_set" = "d" ] || [ "$type_set" = "f" ] || type_set="e"
if [ -"$type_set" "$1" ]; then
  IFS=" " read -ra array_access_stats <<<  "$(ls -ld "$1")"
  if [ "$actual_user" = "${array_access_stats[2]}" ]; then
    access_stats_index="1"
  elif [[ " $actual_groups " == *" ${array_access_stats[3]} "* ]]; then
    access_stats_index="4"
  else
    access_stats_index="7"
  fi
  if ( ! [[ "$permission_set" =~ [r] ]] || [ "${array_access_stats[0]:$access_stats_index:1}" = "r" ] ) &&
     ( ! [[ "$permission_set" =~ [w] ]] || [ "${array_access_stats[0]:($access_stats_index+1):1}" = "w" ] ) &&
     ( ! [[ "$permission_set" =~ [x] ]] || [ "${array_access_stats[0]:($access_stats_index+2):1}" = "x" ] ) ; then
    :;
  else
    [ -d "$1" ] && echo "The directory \"$1\" has not the right permissions for this operation."
    [ -f "$1" ] && echo "The file \"$1\" has not the right permissions for this operation."
    echo "EXITING"
    return 1
  fi
else
  echo "The destination \"$1\" does not exist or does not match the specified type."
  echo "EXITING"
  return 1
fi
}

##  Function "_fix_file_access"
#   This function fixes file context issues.
#   Missing folders are created if missing.
#   Permissions and ownership of files and folders are set as needed.
#
#   Usage: _fix_file_access <Path> <Permissions> <Type>
#          _fix_file_access "/etc/certmanager/certmanager.sh" "rwx" "f"
#
_fix_file_access(){
#$1 Path to check
#$2 Required Permissions (rwx) set to "rwx" if invalid
#$3 Optional type, directory or file (d/f) only neccessary if target is missing, set to "f" if invalid
local permission_set="$2"
local type_set="$3"
local array_access_stats
local actual_user=$(whoami)
local actual_groups=$(groups)
if ([ ${#permission_set} -lt 1 ] || [ ${#permission_set} -gt 3 ]) ||
   [[ $permission_set =~ [^rwx] ]]; then
    permission_set="rwx"
fi
[ "$type_set" = "d" ] || [ "$type_set" = "f" ] || type_set="f"
if [ -e "$1" ] ; then
  IFS=" " read -ra array_access_stats <<<  "$(ls -ld "$1")"
  if [ "$actual_user" = "${array_access_stats[2]}" ]; then
    chmod u="$permission_set" "$1" > /dev/null 2>&1
  else
    echo " "
    echo "The target \"$1\" does already exist but has not the neccessary permissions."
    echo "Because the actual user is not the owner, it is neccessary to use sudo to set the permissions as needed."
    echo "Do you want to continue? (y/n)"
    read -t 10 -r user_decission
    echo " "
    if [ "$user_decission" = "y" ]; then
      if [[ " $actual_groups " == *" ${array_access_stats[3]} "* ]]; then
        sudo chmod g="$permission_set" "$1" > /dev/null 2>&1
      else
        sudo chmod o="$permission_set" "$1" > /dev/null 2>&1
      fi
    fi
  fi
elif [ "$type_set" = "f" ] ; then
  touch "$1" > /dev/null 2>&1
  chmod u="$permission_set" "$1" > /dev/null 2>&1
elif [ "$type_set" = "d" ] ; then
  mkdir -p "$1" > /dev/null 2>&1
  chmod u="$permission_set" "$1" > /dev/null 2>&1
fi
}

##  Function "_load_config"
#   This function loads the configuration from a given filename.
#   This is the configuration for the environment of this script it dosen't hold any certificate data.
#   If the file is valid all values will be loaded into the global variables where they belong.
#
#   Usage: _load_config <Path_config_file> <Variable_list>
#          _load_config "/etc/certmanager/certmanager.conf" "var1 var2 var3"
#
_load_config(){
#$1 Path to certmanager.conf
#$2 List containing all valid configuration-variables
local i=0
local name
local value
_check_file_access "$1" "r" "f" || return 1
while read -r line; do
if [[ "$line" =~ ^[^#]*= ]]; then
        name[i]=`echo "$line" | cut -d'=' -f 1`
        value[i]=`echo "$line" | cut -d'=' -f 2`
        value[i]="${value[i]#\ }"
        value[i]="${value[i]#\"}"
        value[i]="${value[i]%?}"
        value[i]="${value[i]%\"}"

        for config_value in $2; do
        if [ ${config_value} = ${name[i]} ]; then
          eval "$config_value"="${value[i]}"
        fi
        done

        ((i++))
fi
done < "$1"
}


##  Function "_check_cert_format"
#   This function checks if a given string contains a valid certificate.
#
#   Usage: _check_cert_format <String_Variable>
#          _check_cert_format "$certificate"
#
_check_cert_format(){
#$1 Certificate-Content to check
if ! ( [[ "$1" == *"-BEGIN CERTIFICATE-"* ]] && [[ "$1" == *"-END CERTIFICATE-"* ]] ); then
  echo "The returned certificate does not match the expected pattern."
  echo "There is no point in continueing with an invalid certificate."
  echo "EXITING"
  return 1
fi
}

##  Function "_identity"
#   This function checks if a identity-file is accessable.
#   If the file is missing or can't be accessed the user can decide to create a new one.
#   If the configured folder to store the identity-file can't be accessed an error will be returned.
#
#   Usage: _identity <Path_storange_folder>
#          _identity "/etc/certmanager/identity"
#
_identity(){
#$1 Path to folder holding the ID-File
##Test if identity.key exists
#If not ask the user what to do
_check_file_access "$1" "rwx" "d" || return 1
if [ -f "$1/identity.key" ]; then
  _check_file_access "$1/identity.key" "r" "f" || return 1
  echo "Found existing ID-File."
else
  echo "Unable to load your ID-File! $1/identity.key"
  echo " "
  echo "Do you want to create a new one? (y/n)"
  read -r user_decission
  echo " "
  if [ "$user_decission" = "y" ]; then
    openssl genrsa -out "$1/identity.key" 4096
    _check_file_access "$1/identity.key" "r" "f" || return 1
    chmod 400 "$1/identity.key"
    echo "New ID-File was created successfully."
  else
    echo "There is nothing to do without ID-File!"
    echo "EXITING"
    return 1
  fi
fi
}

##  Function "_private"
#   This function creates a private certificate with a given filename.
#   If the configured folder to store the certificate can't be accessed an error will be returned.
#
#   Usage: _private <Path_storange_folder> <Identifier>
#          _private "/etc/certmanager/private" "MyOrganisation"
#
_private(){
#$1 Path to folder holding private certificates
#$2 Filename for current certificate
_check_file_access "$1" "rwx" "d" || return 1
openssl genrsa -out "$1/$2.pem" 4096
_check_file_access "$1/$2.pem" "r" "f" || return 1
echo "New private certificate $1/$2.pem was created successfully."
}

##  Function "_csr"
#   This function creates a CSR certificate with a given filename.
#   If the configured folder to store the certificate can't be accessed an error will be returned.
#
#   Usage: _csr <Path_storange_folder> <Path_configuratoin_folder> <Path_private_certificate_folder> <Identifier>
#          _csr "/etc/certmanager/csr" "/etc/certmanager/config" "/etc/certmanager/private" "MyOrganisation"
#
_csr(){
#$1 Path to folder holding CSR-files
#$2 Path to folder holding configuration-files
#$3 Path to folder holding private certificates
#$4 Filename for current certificate
_check_file_access "$1" "rwx" "d" || return 1
_check_file_access "$2/$4" "r" "f" || return 1
_check_file_access "$3/$4.pem" "r" "f" || return 1
openssl req -config "$2/$4" -new -key "$3/$4.pem" -out "$1/$4.csr"
_check_file_access "$1/$4.csr" "r" "f" || return 1
echo "New CSR-file $1/$4.csr was created successfully."
}

##  Function "_config"
#   This function creates a configuration that contains all informations for one identifier.
#   If the configured folder to store the file can't be accessed an error will be returned.
#
#   Usage: _config <Path_storange_folder>
#          _config "/etc/certmanager/config"
#
_config(){
#$1 Path to folder holding configuration-files
_check_file_access "$1" "rwx" "d" || return 1

local csr_extended_countryName="DE"
local csr_extended_stateOrProvinceName="N/A"
local csr_extended_localityName="N/A"
local csr_extended_postalCode="N/A"
local csr_extended_streetAddress="N/A"
local csr_extended_organizationalUnitName="N/A"

echo "Please choose a profilename for your configuration. (MyOrganization)"
read -r user_profile
echo " "
echo "Please enter the primary domain. (MyDomain.com)"
read -r user_domain
echo " "
echo "Please enter the secondary domains. (www,webmail,files)"
read -r user_subdomain
echo " "

echo "Do you want to enter extended organization details?"
echo "These details are NOT used by Let's Encrypt, but will be included in your request."
echo "(y/n)"
read -r user_decission
echo " "

if [ "$user_decission" = "y" ]; then
  echo " "
  echo "Please enter \"countryName\" for your organization \"$user_profile\". (DE)"
  read -r csr_extended_countryName
  echo " "
  echo "Please enter \"stateOrProvinceName\" for your organization \"$user_profile\". (Bavaria)"
  read -r csr_extended_stateOrProvinceName
  echo " "
  echo "Please enter \"localityName\" for your organization \"$user_profile\". (Building 3)"
  read -r csr_extended_localityName
  echo " "
  echo "Please enter \"postalCode\" for your organization \"$user_profile\". (90402)"
  read -r csr_extended_postalCode
  echo " "
  echo "Please enter \"streetAddress\" for your organization \"$user_profile\". (Mainstreet 1)"
  read -r csr_extended_streetAddress
  echo " "
  echo "Please enter \"organizationalUnitName\" for your organization \"$user_profile\". (IT Department)"
  read -r csr_extended_organizationalUnitName
  echo " "
fi

IFS="," read -ra array_subdomains <<< "$user_subdomain"
string_subdomains=""
for i in "${array_subdomains[@]}"; do
  string_subdomains="$string_subdomains$i.$user_domain,"
done
[[ $string_subdomains == *, ]] && string_subdomains="${string_subdomains::-1}"

echo "This are the details you entered, is everything fine?"
echo "Profilename:	$user_profile"
echo "Domainname:	$user_domain"
echo "Subdomains:	$string_subdomains"
[ "$user_decission" = "y" ] && echo " "
[ "$user_decission" = "y" ] && echo "countryName = \"$csr_extended_countryName\""
[ "$user_decission" = "y" ] && echo "stateOrProvinceName = \"$csr_extended_stateOrProvinceName\""
[ "$user_decission" = "y" ] && echo "localityName = \"$csr_extended_localityName\""
[ "$user_decission" = "y" ] && echo "postalCode = \"$csr_extended_postalCode\""
[ "$user_decission" = "y" ] && echo "streetAddress = \"$csr_extended_streetAddress\""
[ "$user_decission" = "y" ] && echo "organizationalUnitName = \"$csr_extended_organizationalUnitName\""
echo "(y/n)"
read -r user_decission
echo " "

if [ "$user_decission" = "y" ]; then
  final_string_subdomains=${string_subdomains//","/",DNS:"}
{
echo "[ req ]
default_md = sha512
prompt = no
encrypt_key = no
distinguished_name = req_distinguished_name"
[ -z "$final_string_subdomains" ] ||  echo "req_extensions = v3_req"
echo "[ req_distinguished_name ]
countryName = \"$csr_extended_countryName\"
stateOrProvinceName = \"$csr_extended_stateOrProvinceName\"
localityName = \"$csr_extended_localityName\"
postalCode = \"$csr_extended_postalCode\"
streetAddress = \"$csr_extended_streetAddress\"
organizationName = \"$user_profile\"
organizationalUnitName = \"$csr_extended_organizationalUnitName\"
commonName = \"$user_domain\""
[ -z "$final_string_subdomains" ] ||  echo "[ v3_req ]"
[ -z "$final_string_subdomains" ] ||  echo "subjectAltName = DNS:$final_string_subdomains"
} > "$1/$user_profile"
else
  echo "There is nothing to do without valid configuration!"
  echo "EXITING"
  return 1
fi

_check_file_access "$1/$user_profile" "r" "f" || return 1
echo "New configuration $1/$user_profile was created successfully."
}

##  Function "_config_available"
#   This function retruns a list of all available configurations.
#   If the storage folder can't be accessed it will return an empty list.
#
#   Usage: _config_available <Path_storange_folder>
#          _config_available "/etc/certmanager/config"
#
_config_available(){
#$1 Path to folder holding configuration-files
local config_list=""
while IFS= read -r -d '' config; do
  config_list="$config_list "${config//$1\/""}
done < <(find "$1" -type f -print0)
echo "$config_list"
}

##  Function "_public"
#   This function creates a public certificate with a given filename.
#   The certificate will be signed by "Let's Encrypt" and contain their intermediate certificate.
#   If the configured folder to store the certificate can't be accessed an error will be returned.
#
#   Usage: _public <Path_acme_handler> <Path_identity_folder> <Path_csr_certificate_folder> <Path_storange_folder> <Path_acme_challange_files> <Identifier>
#          _public "/etc/certmanager/acme_tiny.py" "/etc/certmanager/identity" "/etc/certmanager/csr" "/etc/certmanager/public" "/etc/certmanager/acme" "MyOrganisation"
#
_public(){
#$1 Path to ACME Script
#$2 Path to folder holding the ID-File
#$3 Path to folder holding CSR-files
#$4 Path to folder holding public certificates
#$5 Path to folder ACME challange files
#$6 Filename for current certificate
_check_file_access "$1" "rx" "f" || return 1
_check_file_access "$2/identity.key" "r" "f" || return 1
_check_file_access "$3/$6.csr" "r" "f" || return 1
_check_file_access "$4" "rwx" "d" || return 1
_check_file_access "$5" "rwx" "d" || return 1
#Run the ACME-Handler and save the returned values
echo "Request a new public certificate from Let's Encrypt."
local return_value_acme
return_value_acme=$(python "$1" --account-key "$2/identity.key" --csr "$3/$6.csr" --acme-dir "$5")
_check_cert_format "$return_value_acme" || return 1
echo "$return_value_acme" > "$4/$6.pem"
_check_file_access "$4/$6.pem" "r" "f" || return 1
echo "New public certificate $4/$6.pem was created successfully."
}


################################################################################
##  Mainscript
#

clear

## Check if this script is running as root
current_user=$(whoami)
if [ "$current_user" = "root" ]; then
  echo "You are logged in as $current_user."
  echo "C'mon thats a bad idea."
  exit
fi

## Get the actual script location
script_DIR=$(dirname "$0")

## Check if a valid configuration-file is present and load it
_load_config "$script_DIR/certmanager.conf" "identity_DIR config_DIR certstorage_DIR acmechallange_DIR acmetiny_FILE ssh_address_list ssh_user" || exit

## Check if all variables are defined and valid
[ -z "$identity_DIR" ] && echo "The variable \"identity_DIR\" is not set." && echo "EXITING" && exit
identity_DIR="${identity_DIR%\/}"
_check_file_access "$identity_DIR" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR" "rwx" "d"
_check_file_access "$identity_DIR" "rwx" "d" || exit

[ -z "$config_DIR" ] && echo "The variable \"config_DIR\" is not set." && echo "EXITING" && exit
config_DIR="${config_DIR%\/}"
_check_file_access "$config_DIR" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR" "rwx" "d"
_check_file_access "$config_DIR" "rwx" "d" || exit

[ -z "$certstorage_DIR" ] && echo "The variable \"certstorage_DIR\" is not set." && echo "EXITING" && exit
certstorage_DIR="${certstorage_DIR%\/}"
_check_file_access "$certstorage_DIR" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR" "rwx" "d"
_check_file_access "$certstorage_DIR" "rwx" "d" || exit
_check_file_access "$certstorage_DIR/CSR" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR/CSR" "rwx" "d"
_check_file_access "$certstorage_DIR/public" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR/public" "rwx" "d"
_check_file_access "$certstorage_DIR/private" "rwx" "d" > /dev/null 2>&1 || _fix_file_access "$identity_DIR/private" "rwx" "d"

[ -z "$acmechallange_DIR" ] && echo "The variable \"acmechallange_DIR\" is not set." && echo "EXITING" && exit
acmechallange_DIR="${acmechallange_DIR%\/}"
_check_file_access "$acmechallange_DIR" "rx" "d" || exit

[ -z "$acmetiny_FILE" ] && acmetiny_FILE="$script_DIR/include/acme-tiny/acme_tiny.py"
_check_file_access "$acmetiny_FILE" "rx" "f" || exit


case "$1" in
create)
_identity "$identity_DIR" || exit
_config "$config_DIR" || exit

user_profile=$(ls -t "$config_DIR" | head -n1)
_private "$certstorage_DIR/private" "$user_profile" || exit
_csr "$certstorage_DIR/CSR" "$config_DIR" "$certstorage_DIR/private" "$user_profile" || exit
_public "$acmetiny_FILE" "$identity_DIR" "$certstorage_DIR/CSR" "$certstorage_DIR/public" "$acmechallange_DIR" "$user_profile" || exit

;;
renew)
_identity "$identity_DIR" || exit
_check_file_access "$config_DIR" "rx" "d" || exit
config_list=$(_config_available "$config_DIR")
[ -z "$config_list" ] && echo "It is necessary to create a profile first." && echo "EXITING" && exit

for user_profile in $config_list; do
  _public "$acmetiny_FILE" "$identity_DIR" "$certstorage_DIR/CSR" "$certstorage_DIR/public" "$acmechallange_DIR" "$user_profile" || exit
done

;;
complete-renew)
_identity "$identity_DIR" || exit
_check_file_access "$config_DIR" "rx" "d" || exit
config_list=$(_config_available "$config_DIR")
[ -z "$config_list" ] && echo "It is necessary to create a profile first." && echo "EXITING" && exit

for user_profile in $config_list; do
  _private "$certstorage_DIR/private" "$user_profile" || exit
  _csr "$certstorage_DIR/CSR" "$config_DIR" "$certstorage_DIR/private" "$user_profile" || exit
  _public "$acmetiny_FILE" "$identity_DIR" "$certstorage_DIR/CSR" "$certstorage_DIR/public" "$acmechallange_DIR" "$user_profile" || exit
done

;;
delete)
_check_file_access "$config_DIR" "rx" "d" || exit
config_list=$(_config_available "$config_DIR")
[ -z "$config_list" ] && echo "It is necessary to create a profile first." && echo "EXITING" && exit

echo "The following configurations are available."
echo "Choose one to proceed."
select user_profile in $config_list; do
  if [[ -n $user_profile ]]; then
    echo "We will proceed with \"$user_profile\""
    break
  else
    echo "Your selection is not valid!"
    echo "EXITING"
    exit
  fi
done

[ -z $user_profile ] && echo "There is no configuration available!" && echo "EXITING" && exit

echo "If you continue all relevant files matching your selection will be deleted!"
echo "Are you shure? (y/n)"
read -r user_decission

if [ "$user_decission" = "y" ]; then
  rm "$config_DIR/$user_profile" &> /dev/null
  rm "$certstorage_DIR/CSR/$user_profile.csr" &> /dev/null
  rm "$certstorage_DIR/private/$user_profile.pem" &> /dev/null
  rm "$certstorage_DIR/public/$user_profile.pem" &> /dev/null

  [ -f "$config_DIR/$user_profile" ] && echo "It was not possible to deleate $config_DIR/$user_profile"
  [ -f "$certstorage_DIR/CSR/$user_profile.csr" ] && echo "It was not possible to deleate $certstorage_DIR/CSR/$user_profile.csr"
  [ -f "$certstorage_DIR/private/$user_profile.pem" ] && echo "It was not possible to deleate $certstorage_DIR/private/$user_profile.pem"
  [ -f "$certstorage_DIR/public/$user_profile.pem" ] && echo "It was not possible to deleate $certstorage_DIR/public/$user_profile.pem"
  echo "DONE"
else
  echo "EXITING"
  exit
fi

;;
sync)
[ -z "$ssh_address_list" ] && echo "The variable \"ssh_address_list\" is not set." && echo "EXITING" && exit
[ -z "$ssh_user" ] && echo "The variable \"ssh_user\" is not set." && echo "EXITING" && exit
echo "Your Certs will be synced via ssh."
echo "If your Clientconfiguration does not support an authentification method, you will be prompted for your password."
for address in $ssh_address_list; do
  rsync --delete -rtoDvze ssh "$certstorage_DIR/" $ssh_user@$address:"$certstorage_DIR/" --chmod=555 --chown=root:root --exclude=*.key --exclude=/CSR/
done
echo "DONE"

;;
*)
echo " "
echo "  Usage $0 {create|renew|complete-renew|delete|sync}"
echo " "
echo "  create:         Creates a new configuration and corresponding certificates, existing files will be overwritten."
echo "                  All new Certs will be signed automatically."
echo " "
echo "  renew:          Generates and signs new public certificates for existing configurations."
echo "                  Existing configurations and private certificates will remain untouched."
echo "                  Can only be used if a valid configuration and private certificate were created before."
echo " "
echo "  complete-renew: Generates and signs new public AND private certificates for existing configurations."
echo "                  Existing configurations will remain untouched."
echo "                  Can only be used if a valid configuration and private certificate were created before."
echo " "
echo "  delete:         Deletes the configuration and private/public certificates of a given name."
echo " "
echo "  sync:           Synchronizes your private and public Certificates with another Server."
echo "                  CSR files may be transmitted as well."
echo " "

;;
esac
